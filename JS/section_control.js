<script>

function addPlus() {
  $('.form-section-title span:contains([+]), .subform-section-title span:contains([+])').click(function() {
    var titleSpan = $(this);
    var title = titleSpan.text();
    var subs = $(titleSpan).parent().parent().children('.form-column, .subform-column');
    titleSpan.text(title.replace("+", "-"));
    subs.show();
    addMinus();
  });
}

function addMinus() {
  $('.form-section-title span:contains([-]), .subform-section-title span:contains([-])').click( function() {
    var titleSpan = $(this);
    var title = titleSpan.text();
    var subs = $(titleSpan).parent().parent().children('.form-column, .subform-column');
    titleSpan.text(title.replace("-", "+"));
    subs.hide();
    addPlus();
  });
}

$(document).ready(function() {
  $('.grid-action-edit').remove();
  /*** Lenyithato section kezeles ***/
  $('span:contains([+])').css("cursor", "pointer").parent().parent().children('.form-column, .subform-column').hide();
  addPlus();
});
</script>