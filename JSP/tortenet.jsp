﻿<%@page contentType="text/html" pageEncoding="UTF-8"
%><%@page import="java.sql.*"
%><%@page import="java.io.*"
%><%@page import="java.util.*"
%><%@page import="java.net.URLDecoder"
%><%@page import="hu.bonafarm.joget.database.QueryManager"
%><%
  String id = URLDecoder.decode(request.getParameter("id").toString().trim(),"UTF-8");
  
  QueryManager qm = null;
  
  StringBuffer ret = new StringBuffer();
  ret.append("<table>");
  ret.append("<thead>");
  ret.append("<tr>");
  ret.append("<th>").append("Név").append("</th>");
  ret.append("<th>").append("Terület").append("</th>");
  ret.append("<th>").append("Megjegyzés").append("</th>");
  ret.append("<th>").append("Dátum").append("</th>");
  ret.append("<th>").append("Döntés").append("</th>");
  ret.append("</tr>");
  ret.append("</thead>");
  ret.append("<tbody>");
  try {
    qm = new QueryManager("jdbc/jwdb");
    String query = "SELECT NVL(c_nev,' ') nev, NVL(c_terulet,'  ') terulet, NVL(c_megj,' ') megj, NVL(c_date,' ') datum, NVL(c_dontes,' ') dontes  FROM app_fd_recept_mcs_g_megj WHERE c_pid = '" + id + "' order by c_date DESC";
    ResultSet rs = qm.executeQuery(query);
    while (rs.next()) {
      ret.append("<tr>");
      ret.append("<td>").append(rs.getString("nev")).append("</td>");
      ret.append("<td>").append(rs.getString("terulet")).append("</td>");
      ret.append("<td>").append(rs.getString("megj")).append("</td>");
      ret.append("<td>").append(rs.getString("datum")).append("</td>");
      ret.append("<td>").append(rs.getString("dontes")).append("</td>");
      ret.append("</tr>");
    }
    ret.append("</tbody></table>");
    out.print(ret);
  } catch (Exception e) {
    e.printStackTrace(new PrintWriter(out));
  } finally {
    qm.close();
  }
%>